package android.rnita.me.lets_bunpai;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText peopleText;
    private EditText moneyText;
    private int[] arrayLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        peopleText = (EditText) findViewById(R.id.people_edit);
        moneyText = (EditText) findViewById(R.id.money_edit);
        ImageButton toResultButton = (ImageButton) findViewById(R.id.toResult_button);
        toResultButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.toResult_button) {
            LayoutInflater inflater = getLayoutInflater();
            final View layout = inflater.inflate(R.layout.dialog, (ViewGroup) findViewById(R.id.alertdialog_layout));

            arrayLL = new int[]{
                    getResources().getIdentifier("people1", "id", getPackageName()),
                    getResources().getIdentifier("people2", "id", getPackageName()),
                    getResources().getIdentifier("people3", "id", getPackageName()),
                    getResources().getIdentifier("people4", "id", getPackageName()),
                    getResources().getIdentifier("people5", "id", getPackageName())
            };
            final int peopleNumber = Integer.parseInt(peopleText.getText().toString());

            // いらない View を非表示に
            for (int i = 5; i > peopleNumber; i--) {
                LinearLayout notll = (LinearLayout) layout.findViewById(arrayLL[i - 1]);
                notll.setVisibility(View.INVISIBLE);
            }

            // Dialog に表示する TextView の内容
            for (int i = 0; i < peopleNumber; i++) {
                LinearLayout ll = (LinearLayout) layout.findViewById(arrayLL[i]);
                TextView textView = (TextView) ll.findViewById(R.id.multiple_number);
                StringBuffer buffer = new StringBuffer();
                String[] alpha = new String[]{
                        Integer.toString(i + 1),
                        "人目"
                };

                for (String str : alpha) {
                    buffer.append(str);
                }
                textView.setText(buffer.toString());
            }

            // AlertDialog を生成
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.dialog_str));
            builder.setView(layout);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // OK ボタンクリック処理
                            Intent intent = new Intent();
                            ArrayList nameList = new ArrayList<>(peopleNumber);
                            ArrayList ageList = new ArrayList<>(peopleNumber);

                            for (int i = 0; i < peopleNumber; i++) {
                                LinearLayout ll = (LinearLayout) layout.findViewById(arrayLL[i]);
                                EditText peopleEdit = (EditText) ll.findViewById(R.id.multiple_name);
                                EditText ageEdit = (EditText) ll.findViewById(R.id.multiple_age);
                                nameList.add(i, peopleEdit.getText().toString());
                                ageList.add(i, ageEdit.getText().toString());
                            }

                            // Log.d("test1", nameList.toString());
                            // Log.d("test2", ageList.toString());
                            // Log.d("test3", moneyText.getText().toString());

                            intent.putExtra("name", nameList);
                            intent.putExtra("age", ageList);
                            intent.putExtra("money", moneyText.getText().toString());
                            intent.setClass(MainActivity.this, ResultActivity.class);
                            startActivity(intent);
                        }
                    }
            );
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()

                    {
                        public void onClick(DialogInterface dialog, int which) {
                            // Cancel ボタンクリック処理
                            dialog.dismiss();
                        }
                    }

            );

            // 表示
            builder.create().

                    show();

        }
    }
}
