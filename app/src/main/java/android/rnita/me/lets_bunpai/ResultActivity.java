package android.rnita.me.lets_bunpai;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public class ResultActivity extends AppCompatActivity {

    private LinearLayout incll, resll;
    private LayoutInflater inflater;
    private TextView resName, resMoney;
    private ArrayList name, age, ageSort, money_give;
    private int money, ram;
    private Random random;
    private ImageButton bbtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        //LinearLayout
        incll = null;
        resll = (LinearLayout) findViewById(R.id.result_layout);

        //LayoutInflaterの準備
        inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Intent intent = getIntent();

        //名前
        name = new ArrayList<>();
        name = intent.getStringArrayListExtra("name");

        //年齢
        age = new ArrayList<>();
        age = intent.getIntegerArrayListExtra("age");
        ageSort = new ArrayList<>(age); //Listへの代入
        Collections.sort(ageSort);

        //若い順
        // Collections.sort(ageSort);
        Log.d("ソート", ageSort.toString());
        Log.d("非ソート", age.toString());


        //金額
        money = Integer.parseInt(intent.getStringExtra("money"));
        money_give = new ArrayList<>(age.size());
        for (int i = 0; i < age.size(); i++) {
            money_give.add(0);
        }

        for (int i = 0; i < age.size(); i++) {
            if (i == age.size() - 1) {
                money_give.set(i, money);
            } else {
                random = new Random();
                ram = random.nextInt(money);
                ram = ram - ram % 100;
                money_give.set(i, ram);


                money = money - (int) money_give.get(i);

            }
            Log.d("金額", Integer.toString((int) money_give.get(i)));
        }


        //include.xmlの内容をメインレイアウトに個追加する
        for (int s = 0; s < ageSort.size(); s++) {

            //LayoutInflaterからViewのインスタンスを取得
            incll = (LinearLayout) inflater.inflate(R.layout.include, null);


            //ソート順に名前を表示
            for (int v = 0; v < name.size(); v++) {
                if (Integer.parseInt(ageSort.get(s).toString()) == Integer.parseInt(age.get(v).toString())) {
                    //名前を追加
                    resName = (TextView) incll.findViewById(R.id.result_name);
                    resName.setText(name.get(v).toString());
                }
            }


            //一人あたりの金額を追加
            resMoney = (TextView) incll.findViewById(R.id.result_money);
            resMoney.setText(money_give.get(s) + "円");


            //ResultActivityのレイアウトに表示
            resll.addView(incll);

        }

        bbtn = (ImageButton) findViewById(R.id.backbtn);
        bbtn.setBackgroundColor(Color.TRANSPARENT);
        bbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
